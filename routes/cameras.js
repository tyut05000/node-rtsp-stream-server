var express = require('express');
const requestmanager = require('../lib/RequestManager')
var router = express.Router();

router.get('/', function (req, res) {
    res.send('Carmeras Server is Runing...');
});

/* GET users listing. */
router.post('/', function (req, res) {
    var cfg = req.body
    let result = new requestmanager().Open(cfg)
    res.json(result)
});

router.post('/close', function (req, res) {
    var cfg = req.body
    new requestmanager().Close(cfg)
    res.json({ state: 'close the rtsp stream success.' })
})



module.exports = router;
