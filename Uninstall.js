let Service = require('node-windows').Service;
 
let svc = new Service({
    name:'node windows server test', //名称
    description: 'The socket.io nodejs server test ',//描述
    script:  './bin/www',//node执行入口 
    execPath:'C:\\Program Files\\nodejs\\node.exe',
    nodeOptions: [
      '--harmony',
      '--max_old_space_size=4096'
    ]
  });

svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
});

svc.uninstall()